FROM tomcat:latest

MAINTAINER Mladen Čikara <mladen.cikara@gmail.com>

VOLUME ["/usr/local/tomcat/conf", "/usr/local/tomcat/logs", "/usr/local/tomcat/webapps"]

CMD ["catalina.sh", "run"]